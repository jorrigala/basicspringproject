package com.basic.domain;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

@Getter
@Setter
@MappedSuperclass
public abstract class Model {

    public static final int ID_LENGTH = 36;
    public static final int ENUM_LENGTH = 256;

    @Id
    @Column(name = "ID", length = 36, nullable = false, updatable = false)
    @Size(max = ID_LENGTH)
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    public Model() {
    }

}
