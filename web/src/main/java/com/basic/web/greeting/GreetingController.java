package com.basic.web.greeting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.basic.service.greeting.GreetingService;


@RestController
public class GreetingController {
	
	@Autowired
	private GreetingService greetingService;

    @RequestMapping(value = "/greeting/{language}", method = RequestMethod.GET)
    public String greeting(@PathVariable("language") String language) {
        return greetingService.getService(language);
    }

}