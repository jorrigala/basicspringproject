package com.basic.service.greeting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.basic.dao.GreetingRepository;
import com.basic.domain.model.language.Greeting;

@Service
public class GreetingService {	
	
	@Autowired
	private GreetingRepository greetingRepository;
	
	public String getService(String language){
		
		Greeting greeting = greetingRepository.findByLanguageIgnoreCase(language);
		if(greeting == null){
			return "Good Morning";			
		}
		
		return greeting.getGreeting();
	}

}
